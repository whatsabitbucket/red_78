require 'rubygems'
require 'bundler/setup'
require 'net/http'
require 'json'

require 'pry'
require 'logger'

Dir['./lib/**/*.rb'].sort.each { |file| require file }

logger = Logger.new(STDOUT)
logger.level = ENV.fetch('LOG_LEVEL') { :debug }.to_sym

# OpenWeather API key
OPEN_WEATHER = ENV['OPEN_WEATHER']

# Handle exit
at_exit do
  # logger.debug { 'Phone number lookup completed.' }
end

# Output to STDOUT
begin
  response = Weather.new(logger: logger)
  output = response.get_weather
  $stdout.puts(output)

rescue StandardError => e
  logger.error(error: 'An unexpected error occurred', message: e.message, backtrace: e.backtrace)
end
