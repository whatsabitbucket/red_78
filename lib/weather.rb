# frozen_string_literal: true

class Weather
  attr_reader :logger
  attr_accessor :client
  # attr_accessor :common_passwords

  def initialize(logger: Logger.new(STDOUT))
    @logger = logger
    @client = URI("https://samples.openweathermap.org/data/2.5/weather?zip=#{ARGV[0]},us&appid=#{OPEN_WEATHER}")
  end

  # @return [String]
  def get_weather
    @weather = JSON.parse(Net::HTTP.get(@client))
    description + ' ' + temp + ' Kelvin'
  end

  # @return [String]
  def description
    @weather['weather'][0]['description']
  end

  # @return [String]
  def temp
    @weather['main']['temp'].to_s
  end

end
